const moment = require('moment');
const { sum,avg } = require('./app/utils/statistic');

// Example usage
const data = [
    { date: '2022-01-15', number: 10, employee: 'John' },
    { date: '2022-01-15', number: 40, employee: 'John' },
    { date: '2022-01-15', number: 15, employee: 'Alice' },
    { date: '2022-01-20', number: 15, employee: 'John' },
    { date: '2022-02-05', number: 20, employee: 'Alice' },
    { date: '2022-02-10', number: 25, employee: 'John' },
    { date: '2023-01-01', number: 30, employee: 'Alice' },
];

const resultSum = sum(data, (i) => i.number, (i) => `${moment(i.date).format('YYYY-MM-DD')}_${i.employee}`);
const resultAvg = avg(data, (i) => i.number, (i) => `${moment(i.date).format('YYYY-MM')}_${i.employee}`);
console.log(resultSum)
console.log(resultAvg)
